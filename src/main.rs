use std::time::SystemTime;
use std::process::{ Command, Output };

use clap::{App, Arg};

struct RbenchConfig {
    iterations: usize,
    show_output: bool,
    commands: Vec<String>,
}

fn main() {
    let config = RbenchConfig::new();

    let mut command = Command::new(&config.commands[0]);
    let command = command.args(&config.commands[1..]);

    let total_time = (0..config.iterations).fold(0, |acc, run| {
        let now = SystemTime::now();
        let out = command.output().unwrap();

        if config.show_output {
            print_info(run, out);
        }

        match now.elapsed() {
            Ok(elapsed) => acc + elapsed.as_millis(),
            Err(_) => acc,
        }
    });

    let avg = total_time as f64 / config.iterations as f64;

    println!(
        "Command took {} milliseconds on average to be completed.",
        avg
    );
}

fn print_info(run: usize, out: Output) {
    println!("Run: {}", run);
    let stdout = String::from_utf8(out.stdout).unwrap();
    let stderr = String::from_utf8(out.stderr).unwrap();

    println!("stdout:\n{}", stdout) ;
    eprintln!("stderr:\n{}", stderr);

    println!();
}

impl RbenchConfig {
    fn new() -> Self {
        let args = App::new("rbench")
            .version("0.2.0")
            .author("Dragomir003 <dragolekovic@gmail.com>")
            .arg(
                Arg::with_name("iterations")
                    .short("i")
                    .long("iterations")
                    .takes_value(true)
                    .value_name("count")
                    .help("Sets number of times program is being ran. Default is 5."),
            )
            .arg(
                Arg::with_name("show-output")
                    .short("o")
                    .long("show-output")
                    .takes_value(false)
                    .help("Shows output of each command ran"),
            )
            .arg(
                Arg::with_name("run")
                    .short("r")
                    .long("run")
                    .takes_value(true)
                    .min_values(1)
                    .help("Runs program with arguments supplied after it"),
            )
            .get_matches();

        let iterations = if let Some(iters) = args.value_of("iterations") {
            iters.parse().unwrap()
        } else {
            5
        };

        let show_output = args.occurrences_of("show-output") > 0;

        let commands = args.values_of("run").unwrap().map(String::from).collect();

        Self {
            iterations,
            show_output,
            commands,
        }
    }
}
