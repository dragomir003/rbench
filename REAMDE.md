# Rbench

**Rbench** is a simple benchmarking tool that runs a program multiple times and
outputs average time program took to run.
